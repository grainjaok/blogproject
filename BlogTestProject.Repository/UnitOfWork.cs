﻿using BlogTestProject.EF;
using BlogTestProject.EF.Models;
using BlogTestProject.Repository.Interfaces;
using BlogTestProject.Repository.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogTestProject.Repository
{
	public class UnitOfWork : IUnitOfWork
	{

		private BlogContext _context;
		private ArticleRepository articleRepository;


		public UnitOfWork()
		{
			_context = new BlogContext();
		}

		/// <summary>
		/// Создание репозитория Articles
		/// </summary>
		public IRepository<Article> Articles
		{
			get
			{
				if (articleRepository == null)
					articleRepository = new ArticleRepository(_context);
				return articleRepository;
			}
		}


		public int SaveChanges()
		{
			return _context.SaveChanges();
		}


		#region Dispose

		private bool disposed = false;

		public virtual void Dispose(bool disposing)
		{
			if (!this.disposed)
			{
				if (disposing)
				{
					return;
				}
				this.disposed = true;
			}
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		#endregion

	}
}
