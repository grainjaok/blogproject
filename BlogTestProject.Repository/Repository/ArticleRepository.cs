﻿using BlogTestProject.EF;
using BlogTestProject.EF.Models;
using BlogTestProject.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogTestProject.Repository.Repository
{
	class ArticleRepository : IRepository<Article>
	{
		/// <summary>
		/// Реализая интерфейса IReposiroty для Article
		/// </summary>
		private BlogContext _context;
		
		public ArticleRepository(BlogContext _context)
		{
			this._context = _context;
		}

		public void Create(Article article)
		{
			_context.Articles.Add(article);
		}

		public void Delete(Article article)
		{
			_context.Articles.Remove(article);
		}

		public void Delete(int id)
		{
			var articleToDelete = _context.Articles.Find(id);
			if (articleToDelete != null)
			{
				_context.Articles.Remove(articleToDelete);
			}
		}

		public Article GetById(int id)
		{
			return _context.Articles.Find(id);
		}

		public IEnumerable<Article> GetAll()
		{
			return _context.Articles;
		}

		public void Update(Article item)
		{			
			_context.Entry(item).State = EntityState.Modified;
		}

	}
}
