﻿using BlogTestProject.EF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogTestProject.Repository.Interfaces
{
	interface IUnitOfWork : IDisposable
	{
		IRepository<Article> Articles { get; }
		int SaveChanges();
	}
}
