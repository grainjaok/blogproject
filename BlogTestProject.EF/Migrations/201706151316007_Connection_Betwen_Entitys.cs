namespace BlogTestProject.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Connection_Betwen_Entitys : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Articles", "AuthorId", c => c.String(maxLength: 128));
            CreateIndex("dbo.Articles", "AuthorId");
            AddForeignKey("dbo.Articles", "AuthorId", "dbo.AspNetUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Articles", "AuthorId", "dbo.AspNetUsers");
            DropIndex("dbo.Articles", new[] { "AuthorId" });
            DropColumn("dbo.Articles", "AuthorId");
        }
    }
}
