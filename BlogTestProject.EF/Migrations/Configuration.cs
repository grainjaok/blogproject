namespace BlogTestProject.EF.Migrations
{
	using System;
	using System.Data.Entity;
	using System.Data.Entity.Migrations;
	using System.Data.Entity.Validation;
	using System.Diagnostics;
	using System.Linq;

	internal sealed class Configuration : DbMigrationsConfiguration<BlogTestProject.EF.BlogContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(BlogTestProject.EF.BlogContext context)
        {
			try
			{
				var seedDataCreator = new SeedDataCreator(context);
				seedDataCreator.Seed();
				context.SaveChanges();

			}
			catch (DbEntityValidationException ex)
			{
				foreach (var error in ex.EntityValidationErrors)
				{
					foreach (var dberror in error.ValidationErrors)
					{
						Debug.WriteLine(dberror.ErrorMessage);
					}
				}
				throw;
			}
			catch (Exception)
			{

				throw;
			}
		}
    }
}
