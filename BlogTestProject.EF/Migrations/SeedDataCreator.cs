﻿using BlogTestProject.EF.Helpers;
using BlogTestProject.EF.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogTestProject.EF.Migrations
{
	class SeedDataCreator
	{
		private BlogContext context;


		public SeedDataCreator(BlogContext context)
		{
			this.context = context;
		}

		public void Seed()
		{
			AddRoles();
			UpdateOrCreateUser("Admin", "admin@blog.com", "password1", AuthenticationHelper.ROLE_Admin);
			UpdateOrCreateUser("User", "user@blog.com", "password2", AuthenticationHelper.ROLE_USER);
		}



		private void AddRoles()
		{
			var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));

			// Check to see if Role Exists, if not create it
			foreach (string role in AuthenticationHelper.GetAllRoles())
			{
				if (!roleManager.RoleExists(role))
				{
					if (!(roleManager.Create(new IdentityRole(role)).Succeeded))
					{
						throw new Exception("User role: " + role + " was not created!");
					}
				}
			}
		}


		private ApplicationUser UpdateOrCreateUser(string username, string email, string password, string role)
		{
			var manager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));

			var user = manager.FindByName(username);
			if (null == user)
			{
				user = new ApplicationUser()
				{
					UserName = username,
					Email = email,


				};

				manager.Create(user, password);

				context.SaveChanges();

				manager.RemoveFromRoles(user.Id, AuthenticationHelper.GetAllRoles().ToArray());
				manager.AddToRole(user.Id, role);
			}


			return user;
		}



	}
}
