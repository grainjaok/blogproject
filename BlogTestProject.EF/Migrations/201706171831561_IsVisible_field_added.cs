namespace BlogTestProject.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class IsVisible_field_added : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Articles", "IsVisible", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Articles", "IsVisible");
        }
    }
}
