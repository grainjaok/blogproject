﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace BlogTestProject.EF.Helpers
{
	public static class AuthenticationHelper
	{
		
		#region User Roles (constants)	

		public const string ROLE_Admin = "Admin";
		public const string ROLE_USER = "User";

		#endregion
		

		/// <summary>
		/// Gets all roles.
		/// </summary>
		/// <returns>List&lt;String&gt;.</returns>
		public static List<String> GetAllRoles()
		{
			List<String> roles = new List<string>();
			roles.Add(ROLE_Admin);
			roles.Add(ROLE_USER);
			return roles;
		}


		public static bool HasAccessType_Admin(IPrincipal user)
		{
			return DoUserInAccessType(user, ROLE_Admin);
		}

		public static bool HasAccessType_User(IPrincipal user)
		{
			return DoUserInAccessType(user, ROLE_USER);
		}

		#region Private 



		/// <summary>
		///Возвращает true если юзер имеет роль администратора
		/// Otherwise False.
		/// </summary>
		/// <param name="user"></param>
		/// <param name="rolesOfType"></param>
		/// <returns></returns>
		private static bool DoUserInAccessType(IPrincipal user, string rolesOfType)
		{
			if (user.IsInRole(rolesOfType))
				return true;

			return false;
		}

		#endregion

	}
}
