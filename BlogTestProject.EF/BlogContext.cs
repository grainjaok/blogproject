﻿using BlogTestProject.EF.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogTestProject.EF
{
    public class BlogContext : IdentityDbContext<ApplicationUser>
	{
		public BlogContext() : base("BlogContext")
		{
		}
		public DbSet<Article> Articles { get; set; }
	}
}
