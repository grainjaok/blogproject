﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogTestProject.EF.Models
{
	public class Article
	{
		public int Id { get; set; }
		public string Title { get; set; }
		public string ImageUrl { get; set; }
		public string Text { get; set; }
		public bool IsVisible { get; set; }


		public string AuthorId { get; set; }
		[ForeignKey("AuthorId")]
		public virtual ApplicationUser Author { get; set; }
	}
}
