﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlogTestProject.Models
{
	public class UserViewModel
	{
		public bool IsUserAdmin { get; set; }
		public string Name { get; set; }
	}
}