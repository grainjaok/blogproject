﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlogTestProject.Models
{
	public class ArticleViewModel
	{
		public int Id { get; set; }
		public string ImageUrl { get; set; }
		public string Title { get; set; }
		public string Text { get; set; }
		public string AuthorId { get; set; }
		public bool IsVisible { get; set; }
		public HttpPostedFileBase File { get; set; }

	}
}