﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BlogTestProject.Extentions
{
	/// <summary>
	/// Ability to define Roles and Users from an array of strings
	/// </summary>
	[AttributeUsageAttribute(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = true)]
	public class AuthorizeAdvancedAttribute : AuthorizeAttribute
	{
		public string[] RolesArray { get; set; }
		public string[] UsersArray { get; set; }


		public AuthorizeAdvancedAttribute()
		{
		}

		protected override bool AuthorizeCore(HttpContextBase httpContext)
		{
			if (RolesArray != null)
				base.Roles = String.Join(",", RolesArray);

			if (UsersArray != null)
				base.Users = String.Join(",", UsersArray);

			return base.AuthorizeCore(httpContext);
		}

		protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
		{
			if (!filterContext.HttpContext.User.Identity.IsAuthenticated)
			{
				filterContext.Result = new HttpUnauthorizedResult();
			}
			else
			{
				filterContext.Result = new RedirectToRouteResult(new System.Web.Routing.RouteValueDictionary(new { controller = "Home" }));
				//controller = "Error", action = "AccessDenied"
			}

		}
	}
}