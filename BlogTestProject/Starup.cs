﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(BlogTestProject.Startup))]
namespace BlogTestProject
{
	public partial class Startup
	{
		public void Configuration(IAppBuilder app)
		{
			ConfigureAuth(app);
		}
	}
}