﻿using BlogTestProject.EF;
using BlogTestProject.EF.Helpers;
using BlogTestProject.EF.Models;
using BlogTestProject.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace BlogTestProject.Controllers
{
    public class AccountController : Controller
    {
		private readonly BlogContext context = new BlogContext();
		RoleManager<IdentityRole> RoleManager { get;  set; }

		public UserManager<ApplicationUser> UserManager { get; private set; }

		public AccountController()
		{
		
				UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
				RoleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
		}

		[AllowAnonymous]
		public ActionResult Login(string returnUrl)
		{
			ViewBag.ReturnUrl = returnUrl;
			return View(new LoginViewModel());
		}


		/// <summary>
		/// Возвращает представление для редактирования прав пользователей
		/// </summary>
		public ActionResult ManageUsers()
		{
			try
			{
				var listOfUsers = UserManager.Users.ToList();
				var users = listOfUsers.Select(x => new UserViewModel
				{
					IsUserAdmin = x.Roles.Select(y => y.RoleId).FirstOrDefault() == RoleManager.FindByName(AuthenticationHelper.ROLE_Admin).Id,
					Name = x.UserName

				}).ToList();
				return View(users);
			}
			catch (Exception)
			{

				//throw;
			}
			return RedirectToAction("ListArticle","Article");
		
		}

		/// <summary>
		/// Сохранение в базу изминений роли пользователя
		/// </summary>
		public ActionResult EditRole(UserViewModel user)
		{
			try
			{
				var oldUser = UserManager.FindByName(user.Name);
				var oldUserId = oldUser.Id;
				var oldRoleId = oldUser.Roles.SingleOrDefault().RoleId;
				var oldRoleName = RoleManager.Roles.SingleOrDefault(r => r.Id == oldRoleId).Name;
				var newRoleName = RoleManager.Roles.Where(x => x.Id != oldRoleId).FirstOrDefault().Name;
				if (oldRoleName != newRoleName)
				{
					UserManager.RemoveFromRole(oldUserId, oldRoleName);
					UserManager.AddToRole(oldUserId, newRoleName);
					
				}			

				return RedirectToAction("ManageUsers", "Account");
			}
			catch (Exception)
			{

				//throw;
			}
			return RedirectToAction("ListArticle", "Article");
		}


		[AllowAnonymous]
		public ActionResult LogOut()
		{
			AuthenticationManager.SignOut();
			return RedirectToAction("Index", "Home", new { area = "" });
		}

		[HttpPost]
		[AllowAnonymous]
		[ValidateAntiForgeryToken]
		public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
		{

			if (ModelState.IsValid)
			{
				try
				{
					if (UserManager != null)
					{
						var user = await UserManager.FindAsync(model.UserName, model.Password);
						if (user != null)
						{
							await SignInAsync(user, model.RememberMe);
							return RedirectToLocal(returnUrl);
						}
					}

				}
				catch (Exception)
				{

					//throw;
				}


			}

			// If we got this far, something failed, redisplay form
			return View(model);
		}

		[HttpGet]
		[AllowAnonymous]
		public ActionResult Registration()
		{
			
			return View();
		}

		[HttpPost]
		[AllowAnonymous]
		[ValidateAntiForgeryToken]
		public ActionResult Registration(RegistrationViewModel user)
		{
			try
			{
				if (ModelState.IsValid)
				{
					var userToCreate = new ApplicationUser()
					{
						UserName = user.UserName,
						Email = user.Email

					};

					var result = UserManager.Create(userToCreate, user.Password);

					if (result.Succeeded)
					{
						UserManager.AddToRole(userToCreate.Id, AuthenticationHelper.ROLE_USER);
						return RedirectToAction("Index","Home");
					}
				}
			}
			catch (Exception)
			{
				//throw;
			}
			return View(user);
		}

		private IAuthenticationManager AuthenticationManager
		{
			get
			{
				return HttpContext.GetOwinContext().Authentication;
			}
		}

		private async Task SignInAsync(ApplicationUser user, bool isPersistent)
		{
			AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
			var identity = await UserManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
			AuthenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = isPersistent }, identity);
		}

		private ActionResult RedirectToLocal(string returnUrl)
		{
			if (Url.IsLocalUrl(returnUrl))
			{
				return Redirect(returnUrl);
			}
			else
			{
				return RedirectToAction("Index", "Home");
			}
		}
	}
}