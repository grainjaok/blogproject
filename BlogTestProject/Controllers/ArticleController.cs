﻿using BlogTestProject.EF.Models;
using BlogTestProject.Models;
using BlogTestProject.Repository;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity.EntityFramework;
using BlogTestProject.EF;
using Microsoft.AspNet.Identity.Owin;
using BlogTestProject.EF.Helpers;

namespace BlogTestProject.Controllers
{
	public class ArticleController : Controller
	{
		private readonly UnitOfWork unitOfWork;

		public ArticleController()
		{
			unitOfWork = new UnitOfWork();
		}
		// GET: Article
		public ActionResult Index()
		{
			return View();
		}
		[HttpGet]
		public ActionResult AddArticle()
		{

			return View();

		}
		/// <summary>
		/// Сохранение в базу новой статьи
		/// </summary>
		/// <param name="model"></param>
		/// <returns></returns>
		[HttpPost]
		public ActionResult AddArticle(ArticleViewModel model)
		{
			try
			{
			

				string ImageName = "";
				if (model.File != null)
				{
					ImageName = System.IO.Path.GetFileName(model.File.FileName);
					string physicalPath = Server.MapPath("~/Images/" + ImageName);
					model.File.SaveAs(physicalPath);
				}
				Article articleToCreate = new Article();
				articleToCreate.Title = model.Title;
				articleToCreate.Text = model.Text;
				articleToCreate.ImageUrl = ImageName;
				articleToCreate.AuthorId = System.Web.HttpContext.Current.User.Identity.GetUserId();
				articleToCreate.IsVisible = true;
				unitOfWork.Articles.Create(articleToCreate);
				if (unitOfWork.SaveChanges() > 0)
				{
					return RedirectToAction("ListArticle", "Article");
				}

			}
			catch (Exception)
			{

				//Error message;
			}

			return RedirectToAction("AddArticle", "Article");

		}

		/// <summary>
		/// Возвращает представление для списка статей
		/// </summary>	
		/// <returns></returns>
		[HttpGet]
		public ActionResult ListArticle()
		{
			try
			{
				ViewBag.CurrentLoggedUserId = System.Web.HttpContext.Current.User.Identity.GetUserId();
				var articleList = unitOfWork.Articles.GetAll().Select(x => new ArticleViewModel
				{
					Id = x.Id,
					ImageUrl = x.ImageUrl,
					Text = x.Text,
					Title = x.Text,
					AuthorId = x.AuthorId,
					IsVisible = x.IsVisible
				}).ToList();
				if (User.IsInRole("User"))
				{
					articleList.RemoveAll(x => x.IsVisible.Equals(false));
				}
				
				return View(articleList);
			}
			catch (Exception)
			{

				//Error message;
			}
			return RedirectToAction("Index", "Home");

		}

		public ActionResult HideArticle(int id)
		{
			try
			{
				var articleToHide = unitOfWork.Articles.GetById(id);
				articleToHide.IsVisible = !articleToHide.IsVisible;
				unitOfWork.Articles.Update(articleToHide);
				if (unitOfWork.SaveChanges() > 0)
				{
					return RedirectToAction("ListArticle", "Article");
				}
			}
			catch (Exception)
			{
				//Error message;
			}

			return RedirectToAction("ListArticle","Article");
		}

		/// <summary>
		/// Возвращает представление для редактирования статьи 
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		[HttpGet]
		public ActionResult EditArticle(int id)
		{
			try
			{
				if (id > 0)
				{
					var article = unitOfWork.Articles.GetById(id);
					var articleToEdit = new ArticleViewModel
					{
						ImageUrl = article.ImageUrl,
						Text = article.Text,
						Title = article.Title,
						Id = article.Id

					};
					return View("EditArticle", articleToEdit);
				}
			}
			catch (Exception)
			{

				//Error message;
			}
			return View("ListArticle", "Article");
			
		}

		/// <summary>
		/// Сохранение в базу отредактированной статьи
		/// </summary>
		/// <param name="model"></param>
		/// <returns></returns>
		[HttpPost]
		public ActionResult EditArticle(ArticleViewModel model)
		{			
			try
			{
				Article articleToEdit = unitOfWork.Articles.GetById(model.Id);
				string ImageName = "";
				if (model.File != null)
				{
					ImageName = System.IO.Path.GetFileName(model.File.FileName);
					string physicalPath = Server.MapPath("~/Images/" + ImageName);
					model.File.SaveAs(physicalPath);
					articleToEdit.ImageUrl = ImageName;
				}
				articleToEdit.Title = model.Title;
				articleToEdit.Text = model.Text;

				unitOfWork.Articles.Update(articleToEdit);
				if (unitOfWork.SaveChanges() > 0)
				{
					return RedirectToAction("ListArticle", "Article");
				}
			}
			catch (Exception)
			{

				//Error message;
			}


			return RedirectToAction("EditArticle", "Article", model);
		}

		/// <summary>
		/// Возвращает представление для просмотра статьи 
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		public ActionResult ReadArticle(int id)
		{
			
			try
			{
				Article article = unitOfWork.Articles.GetById(id);
				ArticleViewModel articleToRead = new ArticleViewModel
				{
					ImageUrl = article.ImageUrl,
					Text = article.Text,
					Title = article.Title
				};
				return View(articleToRead);
			}
			catch (Exception)
			{

				//Error message;
			}
			return RedirectToAction("ListArticle", "Article");



		}

		/// <summary>
		/// Удаление статьи
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		public ActionResult DeleteArticle(int id)
		{
			try
			{
				if (id>0)
				{
					unitOfWork.Articles.Delete(id);
					if (unitOfWork.SaveChanges() >0)
					{
						return RedirectToAction("ListArticle", "Article");
					}
					
				}
			}
			catch (Exception)
			{

				//Error message;
			}

			return RedirectToAction("ListArticle", "Article");
		}


	}
}