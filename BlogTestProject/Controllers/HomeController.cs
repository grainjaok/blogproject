﻿using BlogTestProject.EF.Helpers;
using BlogTestProject.Extentions;
using BlogTestProject.Models;
using BlogTestProject.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace BlogTestProject.Controllers
{
	public class HomeController : Controller
	{

		[AuthorizeAdvanced(RolesArray = new string[] { AuthenticationHelper.ROLE_Admin, AuthenticationHelper.ROLE_USER })]
		[HttpGet]
		public ActionResult Index()
		{
			return View();
		}



	}
}